let fs = require("fs");
let path = require("path");

function makingDirectory(directoryName) {
     fs.mkdir(directoryName, function (error) {
          if (error) {
               console.log(error);
          } else {
               console.log("directory is created");
          }
     });
}

function createfile(directory, countfile) {
     let count = 1;
     let intervalId = setInterval(() => {
          let filename = "file" + count + ".json";
          let filePath = path.join(__dirname, "/test", directory, filename);
          if (count === countfile + 1) {
               clearInterval(intervalId);
          } else {
               fs.writeFile(filePath, `${filename}`, function (err) {
                    if (err) {
                         console.log(err);
                    } else {
                         console.log(filePath + "file is created");
                    }
               });
          }
          count += 1;
     }, 1);
}

function deletefile(directoryName) {
     let pathData = path.join(__dirname, "/test/", directoryName);

     fs.readdir(pathData, (err, data) => {
          if (err) {
               console.log(err);
          } else {
               let arraydata = data.map((eachfile) => {
                    let pathdata = "/test/" + directoryName + "/" + eachfile;
                    let filepath = path.join(__dirname, pathdata);
                    fs.unlink(filepath, (err) => {
                         if (err) {
                              console.log(err);
                         } else {
                              console.log(`${eachfile} deleted succesfully`);
                         }
                    });
                    return true;
               });
          }
     });
}

function callfunction(directoryName, countfile) {
     makingDirectory(directoryName);
     createfile(directoryName, countfile);
     setTimeout(() => {
          deletefile(directoryName);
     }, 5000);
}

module.exports = callfunction;
