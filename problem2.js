let fs = require("fs");
function operationOnFile(filePath) {
     function readingfile(filePath) {
          fs.readFile(filePath, "utf-8", (err, data) => {
               if (err) {
                    console.log(err);
               } else {
                    console.log(data);
                    uppercase(data);
               }
          });
     }
     readingfile(filePath);

     function uppercase(data) {
          let filename = "convertUpperCase.txt";
          let filePath = "/home/prasan/fscallback/" + filename;
          let filedata = data.toUpperCase();
          fs.writeFile(filePath, filedata, (err) => {
               if (err) {
                    console.log(err);
               } else {
                    console.log("file has been converted");
               }
          });
          toLowercase(filedata);
          storingName(filename);
     }
     function toLowercase(upperCaseFile) {
          let filedatalower = upperCaseFile.toLowerCase();
          let splittedFile = filedatalower.split(".").join("\n");
          let filename = "convertToLowercase.txt";
          let filePath = "/home/prasan/fscallback/" + filename;
          fs.writeFile(filePath, splittedFile, (err) => {
               if (err) {
                    console.log(err);
               } else {
                    console.log("file has been converted to lowercase");
               }
          });
          storingName(filename);
          sorting(filedatalower);
     }
     function sorting(file) {
          fs.readFile(file, "utf-8", (err, data) => {
               if (err) {
                    console.log(err);
               } else {
                    console.log(data);
               }
               let sortingfile = file.split(" ");
               let sortingdata = sortingfile.sort();
               let filename = "sortedFiles.txt";
               let filePathWithName = `/home/prasan/fscallback/${filename}`;
               fs.writeFile(filePathWithName, sortingdata, (err) => {
                    if (err) {
                         console.log(err);
                    } else {
                         console.log(filename + "created file successfully");
                    }
               });
               storingName(filename);
          });
     }
     function storingName(filename1) {
          let fileNames = "filenames.txt";
          let filePath = `/home/prasan/fscallback/${fileNames}`;
          let content = `${filename1} `;
          fs.appendFile(filePath, content, (err) => {
               if (err) {
                    console.log(err);
               } else {
                    console.log(" file has been created succefully");
               }
          });
     }
     setTimeout(function deletingfilesAndRead() {
          fs.readFile("/home/prasan/fscallback/filenames.txt","utf-8",
               (err, data) => {
                    if (err) {
                         console.log(err);
                    } else {
                         let filePath = data.split(" ");
                         let deleted = filePath.filter((filename) => {
                                   filename.trim();
                                   return filename !== "";
                              })
                              .map((deletingfile) => {
                                   let fileNameToBeDeleted ="/home/prasan/fscallback/" + deletingfile;
                                   fs.unlink(fileNameToBeDeleted, (err) => {
                                        if (err) {
                                             console.log(err);
                                        } else {
                                             console.log("files deleted successfully");
                                        }
                                   });
                              });
                    }
               }
          );
     }, 3000);
}
module.exports = operationOnFile;
